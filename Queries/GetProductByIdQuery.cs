﻿using FlinndalProducts.Dtos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlinndalProducts.Queries
{
    public class GetProductByIdQuery : IRequest<ProductReadDto>
    {
        public Guid Id { get; }

        public GetProductByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
