using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using FlinndalProducts.Models;

namespace FlinndalProducts.Data
{
    public interface IProductsRepo
    {
      Task<Product> GetProductById(Guid guid);

       Task<List<Product>> GetAllProducts();

       Task<Product> AddProduct(Product product);
        Product UpdateProduct(Product product);


        List<Product> AddProductList(List<Product> product);

        
        Task DeleteProduct(Product product);
        bool SaveChanges();
    }
}