﻿using FlinndalProducts.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlinndalProducts.Data
{
    public class ProductsRepo : IProductsRepo
    {
        private readonly FlinndalDBContext _context;

        public ProductsRepo(FlinndalDBContext context)
        {
            _context = context;
        }

        public async Task<List<Product>> GetAllProducts()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<Product> GetProductById(Guid guid)
        {
            return await _context.Products.FirstOrDefaultAsync(p => p.ProductId == guid);
        }

        public async Task<Product> AddProduct(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
            return product;
        }
        public Product UpdateProduct(Product product)
        {           
            _context.SaveChanges();
            return product;
        }

        public List<Product> AddProductList(List<Product> product)
        {
            foreach (var item in product)
            {
                _context.Products.Add(item);
         
            }
            _context.SaveChanges();
            return product;
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

       public async Task DeleteProduct(Product product)
        {            
            _context.Products.Remove(product);
           await _context.SaveChangesAsync();
        }
    }
}
