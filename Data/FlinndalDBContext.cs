using FlinndalProducts.Models;
using Microsoft.EntityFrameworkCore;

namespace FlinndalProducts.Data
{
    public class FlinndalDBContext : DbContext
    {
        public FlinndalDBContext(DbContextOptions<FlinndalDBContext> options) : base(options)  
        {  
            
        }  
        
       public DbSet<Product> Products { get; set; }  
    }
}