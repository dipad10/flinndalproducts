﻿using AutoMapper;
using FlinndalProducts.Dtos;
using FlinndalProducts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlinndalProducts.Commands;

namespace FlinndalProducts.Profiles
{
    public class FlinndalProductsProfile : Profile
    {
        public FlinndalProductsProfile()
        {
            CreateMap<Product, ProductReadDto>();
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductUpdateDto, Product>();
            CreateMap<Product, ProductUpdateDto>();
            CreateMap<CreateProductCommand, Product>();

        }
    }
}
