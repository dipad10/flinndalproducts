﻿using AutoMapper;
using FlinndalProducts.Data;
using FlinndalProducts.Dtos;
using FlinndalProducts.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FlinndalProducts.Handlers
{
    public class GetProductByIdHandler : IRequestHandler<GetProductByIdQuery, ProductReadDto>
    {
        private readonly IMapper _mapper;
        private readonly IProductsRepo _productrepo;

        public GetProductByIdHandler(IMapper mapper, IProductsRepo productsrepo)
        {
            _mapper = mapper;
            _productrepo = productsrepo;

        }
        public async Task<ProductReadDto> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var product = await _productrepo.GetProductById(request.Id);
            if (product == null) return null;

            var response = _mapper.Map<ProductReadDto>(product);
            return response;
        }
    }
}
