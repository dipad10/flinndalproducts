﻿using AutoMapper;
using FlinndalProducts.Data;
using FlinndalProducts.Dtos;
using FlinndalProducts.Models;
using FlinndalProducts.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FlinndalProducts.Handlers
{
    public class GetAllProductsHandler : IRequestHandler<GetAllProductsQuery, List<ProductReadDto>>
    {
        private readonly IMapper _mapper;
        private readonly IProductsRepo _productrepo;



        public GetAllProductsHandler(IMapper mapper, IProductsRepo productsrepo)
        {
            _mapper = mapper;
            _productrepo = productsrepo;

        }

        public async Task<List<ProductReadDto>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _productrepo.GetAllProducts();
            return _mapper.Map<List<ProductReadDto>>(products);

        }
    }
}
