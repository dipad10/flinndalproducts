using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FlinndalProducts.Commands;
using FlinndalProducts.Data;
using FlinndalProducts.Dtos;
using FlinndalProducts.Models;
using MediatR;

namespace FlinndalProducts.Handlers.CommandHandlers
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Product>
    {
        
        private readonly IMapper _mapper;
        private readonly IProductsRepo _productrepo;

        public CreateProductCommandHandler(IMapper mapper, IProductsRepo productrepo)
        {
            _mapper = mapper;
            _productrepo = productrepo;
        }
        public async Task<Product> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = _mapper.Map<Product>(request);
            var result = await _productrepo.AddProduct(product);
            return result;
        }
    }
}