﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlinndalProducts.Dtos
{
    public class ProductReadDto
    {
      
      
        public Guid ProductId { get; set; }
       
        public double Price { get; set; }
       
        public string Currency { get; set; }
      
        public string Name { get; set; }
       
        public string ProductGroupId { get; set; }
    }
}
