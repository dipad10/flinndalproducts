﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FlinndalProducts.Dtos
{
    public class ProductCreateDto
    {

        [Required]
        public Guid ProductId { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ProductGroupId { get; set; }
    }
}
