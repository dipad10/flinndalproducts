﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace FlinndalProducts.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public Guid ProductId { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ProductGroupId { get; set; }
    }
}
