﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FlinndalProducts.Commands;
using FlinndalProducts.Data;
using FlinndalProducts.Dtos;
using FlinndalProducts.Models;
using FlinndalProducts.Queries;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FlinndalProducts.Controllers.v1
{
    //api/Products
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    [Produces("application/json")]
    public class ProductsController : ControllerBase
    {
       
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductsRepo _productsrepo;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public ProductsController(ILogger<ProductsController> logger, IProductsRepo productsrepo, IMapper mapper, IMediator mediator)
        {
            _logger = logger;
            _productsrepo = productsrepo;
            _mapper = mapper;
            _mediator = mediator;
        }
         
        
         [HttpGet]
         public async Task<ActionResult<List<ProductReadDto>>> GetAllProducts()
         {
            var query = new GetAllProductsQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
         }

        [HttpGet("{id}", Name = "GetProductByid")]
        public async Task<ActionResult<ProductReadDto>> GetProductByid(Guid id)
        {
            var query = new GetProductByIdQuery(id);
            var result = await _mediator.Send(query);

            return result != null ? (ActionResult) Ok(result) : NotFound();
            
        }


        [HttpPost]
         public async Task<ActionResult> CreateProduct(CreateProductCommand command)
         {
             var result = await _mediator.Send(command);
             return CreatedAtRoute(nameof(GetProductByid), new {id = result.ProductId}, result);

         }

        [HttpPost]
        public ActionResult CreateListOfProduct(ProductCreateDto[] productdto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var productlist = _mapper.Map<List<Product>>(productdto);
                 
            _productsrepo.AddProductList(productlist);
            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateProduct(Guid id, ProductUpdateDto productupdatedto)
        {
            if (!ModelState.IsValid) return BadRequest(productupdatedto);
            var productFromRepo = await _productsrepo.GetProductById(id);
            if (productFromRepo == null) return NotFound();
            _productsrepo.UpdateProduct(_mapper.Map(productupdatedto, productFromRepo));
            return NoContent();

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProductById(Guid id)
        {         
            var product = await _productsrepo.GetProductById(id);
            if (product == null)
            {
                return NotFound();
            }
            await _productsrepo.DeleteProduct(product);
            return NoContent();           
        }
        [HttpPatch("{id}")]
        public async Task<ActionResult> PartialProductUpdate(Guid id, JsonPatchDocument<ProductUpdateDto> patchDocument)
        {
            var productFromRepo = await _productsrepo.GetProductById(id);
            if (productFromRepo == null) return NotFound();
            var productToPatch = _mapper.Map<ProductUpdateDto>(productFromRepo);
            patchDocument.ApplyTo(productToPatch, ModelState);
            if (!TryValidateModel(productToPatch)) return ValidationProblem(ModelState);
            
            _productsrepo.UpdateProduct(_mapper.Map(productToPatch, productFromRepo));

            return NoContent();
        }

    }
}
